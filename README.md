# Ansible Role Development Workflow

Jupyter notebook to present Ansible Role Development Workflow at ICS.

## License

This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
